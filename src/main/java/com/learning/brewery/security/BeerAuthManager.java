package com.learning.brewery.security;

import com.learning.brewery.domain.security.User;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;


@Component
public class BeerAuthManager {

    public boolean checkBeerOrganization(Authentication authentication, Integer userId) {
        User authentificatedUser = (User) authentication.getPrincipal();

        return authentificatedUser.getId().equals(userId);
    }
}
